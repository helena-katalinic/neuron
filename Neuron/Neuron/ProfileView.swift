

import SwiftUI

struct ProfileView: View {
    var body: some View {
        List{
            HStack{
            
            Section{
                Text(User.MOCK_USER.initials)
                    .font(.title)
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
                    .frame(width: 72, height: 72)
                    .background(Color(.systemGray3))
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                
                VStack(alignment: .leading, spacing: 4)
                {
                    Text(User.MOCK_USER.name)
                        .font(.subheadline)
                        .fontWeight(.semibold)
                        .padding(.top, 4)
                    
                    Text(User.MOCK_USER.email)
                        .font(.subheadline)
                        .fontWeight(.semibold)
                    
                    
                }
            }
                
 }
            Section("General"){
                HStack{
                    SettingsRowView(
                        imageName: "gear",
                        title: "Version",
                        tintColour: Color(.systemGray))
                    
                    Spacer()
                    Text("1.0.0")
                }
            
        }
            Section("Account"){
                
                Button{
                    print("Sign out..")
                }
            label:{
                SettingsRowView(
                    imageName: "arrow.left.circle.fill",
                    title: "Sign out",
                    tintColour: .red)
                
            }
                Button{
                    print("Delete account")
                }
            label:{
                SettingsRowView(
                    imageName: "xmark.circle.fill",
                    title: "Delete account",
                    tintColour: .red)
                
            }
                Button{
                    print("Delete account")
                }
            label:{
                SettingsRowView(
                    imageName: "circle.fill",
                    title: "Progress",
                    tintColour: .purple)
                
            }
                Button{
                    print("Delete account")
                }
            label:{
                SettingsRowView(
                    imageName: "circle.fill",
                    title: "Planner",
                    tintColour: .purple)
                
            }
                
                
            }
        }
    }
}

#Preview {
    ProfileView()
}
