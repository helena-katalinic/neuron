//
//  Task.swift
//  Neuron
//
//  Created by student on 05.02.2024..
//

import SwiftUI
import SwiftData



@Model
class Task: Identifiable {
    var id : UUID
    var title: String
    var caption: String
    var date: Date
    var isCompleted : Bool
    var tint: String
    
    init(id: UUID = .init() , title: String, caption: String, date: Date = .init() , isCompleted: Bool = false, tint: String) {
        self.id = id
        self.title = title
        self.caption = caption
        self.date = date
        self.isCompleted = isCompleted
        self.tint = tint
    }
    var tintColor: Color
    {
        switch tint {
        case "taskColour1" : return .taskColour1
        case "taskColour2" : return .taskColour2
        case "taskColour3" : return .taskColour3
        case "taskColour4" : return .taskColour4
        default: return .black
            
    }
        
    }
    
    
}
extension Date
{
    static func updateHour(_ value: Int) -> Date
    {
        let calendar = Calendar.current
        return calendar.date(byAdding: .hour, value: value, to: .init()) ?? .init()
    }
    
}




