//
//  NewTask.swift
//  Neuron
//
//  Created by student on 05.02.2024..
//

import SwiftUI
import SwiftData


struct NewTask: View {
    @Environment(\.dismiss) var dismiss
    @State private var taskTitle: String = ""
    @State private var taskCaption: String = ""
    @State private var taskDate: Date = .init()
    @State private var taskColor: String = "taskColour1"
    
    @Environment(\.modelContext) private var context
    
    var body: some View {
       
        VStack(alignment: .leading, content: {
            VStack(alignment: .leading, content: {
                Label("Add new Task", systemImage:  "arrow.left").onTapGesture {
                    dismiss()
                }
            })
            .hSpacing(.leading)
            .padding(30)
            .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
            .background
            {
                Rectangle().fill(.gray.opacity(0.1))
                    .clipShape(.rect(bottomLeadingRadius: 30, bottomTrailingRadius: 30))
            }.hSpacing(.leading)
            ///
            ///
            VStack(alignment: .leading,spacing: 30, content: {
                VStack(spacing: 10, content:
                        {
                    TextField("Your Task Title", text: $taskTitle)
                    Divider()
                })
                VStack(spacing: 10, content:
                        {
                    TextField("Your Task Title", text: $taskCaption)
                    Divider()
                })
                
                VStack(alignment: .leading, content:{
                    Text("Timeline").font(.title2)
                    
                    DatePicker("", selection: $taskDate)
                        .datePickerStyle(.graphical)
                    
                })
                
                VStack(alignment: .leading,spacing: 20, content:{
                    Text("Task color").font(.title3)
                    
                    let colors: [String] = (1...4).compactMap
                    {
                        index -> String in
                        return "taskColour \(index)"
                    }
                    HStack(spacing: 10, content:
                            {
                        ForEach(colors, id:\.self)
                        {
                            color in Circle()
                                .hSpacing(.center).onTapGesture {
                                    withAnimation(.snappy)
                                    {
                                        taskColor = color
                                    }
                                }
                        }
                        
                        
                    })
                })
                Button {
                    let task = Task( title: taskTitle,  caption: taskCaption ,date:taskDate ,tint: taskColor)
                    do{
                        context.insert(task)
                        try context.save()
                        dismiss()//when done end the task
                    }
                    catch{print(error.localizedDescription)}
                    
                }
            label: {
                Text("CreateTask").frame(maxWidth: .infinity).frame(height: 60).hSpacing(.bottom)
            }
                
            })
        
            })
            .vSpacing(.top)
        }
               }
               
                
#Preview {
    ContentView()
}
