import SwiftUI

struct LogIn: View {
    
    @State private var email = ""
    @State private var password = ""
    
    
    var body: some View {
        NavigationStack{
            VStack{
                Spacer()
                VStack{
                    InputView(text: $email,
                              title: "Email Address",
                              placeholder: "nameVgm,al.com")
                    
                    .autocapitalization(.none)
                    .background(.white)
                    .frame(width: .infinity, height: .infinity)
                    .overlay(
                        RoundedRectangle(cornerRadius: 20)
                            .stroke(Color.gray, lineWidth: 2)
                            .opacity(0.5)
                    )
                    
                    InputView(text: $password,
                              title: "Password",
                              placeholder: "Enter your password",
                              isSecureFiled: true)
                    .frame(width: .infinity, height: .infinity)
                    .background(.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 20)
                            .stroke(Color.gray, lineWidth: 2)
                            .opacity(0.8)
                        
                    )
                    
                }.padding(.horizontal)
                    .padding(.top,12)
                
                Button{
                    
                }label:
                {
                    HStack{
                        Text("SIGN IN")
                            .fontWeight(.semibold)
                        Image(systemName: "arrow.right")
                    }
                }.frame(width: 250, height: 35).background(.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 20)
                            .stroke(Color.gray, lineWidth: 2)
                            .opacity(0.9)
                        
                    )
                
                
                Spacer()
                
                NavigationLink{
                    RegistrationView()
                        .navigationBarBackButtonHidden(true)
                } label:
                {
                    Text("Don't have an accoint?")
                    Text("Sign up").fontWeight(.bold)              }
                
            }.background(Image("Bro").imageScale(.small)
                .clipShape(Circle())
                .frame(width: 10, height: 10))
            .opacity(0.7)
            
        }
    }
}
struct NavigationPrimary_Previews: PreviewProvider {
    static var previews: some View {
        LogIn()
    }
}
